//
//  DropletsEmitter.swift
//  Water Droplet
//
//  Created by innovapost on 2018-02-28.
//  Copyright © 2018 innovapost. All rights reserved.
//

import UIKit

class Droplets {
  let layer: CAEmitterLayer
  let initialVelocity = CGFloat(200)
  let yAcceleration = CGFloat(100)
  let lifeTime = Float(1)
  let centerX: CGFloat
  init(at point: CGPoint, with size: CGSize) {
    centerX = point.x
    layer = CAEmitterLayer()
    layer.emitterPosition = point
    layer.emitterSize = size
    layer.emitterShape = kCAEmitterLayerPoint

    
    let fallingDroplet = CAEmitterCell()
    fallingDroplet.contents = UIImage(named: "droplet")?.cgImage
    fallingDroplet.birthRate = 1
    fallingDroplet.lifetime  = lifeTime
    fallingDroplet.scale = 0.04
    fallingDroplet.velocity = initialVelocity //helps to see what direction the cells are moving to
    fallingDroplet.emissionLongitude = CGFloat(90).toRadians() // since the shape is kCAEmitterLayerPoint
    fallingDroplet.yAcceleration = yAcceleration //helps to accelerate the droplets in y direction
    fallingDroplet.spinRange = CGFloat(45).toRadians()
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
      self.layer.addSublayer(self.createMiniDropletsSubLayer())
    }
    
    layer.emitterCells = [fallingDroplet]
  }
  
  func createMiniDropletsSubLayer() -> CAEmitterLayer {
    let layer = CAEmitterLayer()
    let centerY = initialVelocity + (0.5 * yAcceleration * CGFloat(lifeTime) * CGFloat(lifeTime))
  
    layer.position = CGPoint(x: centerX, y: centerY)
    layer.emitterSize = CGSize(width: 50, height: 400)
    layer.emitterShape = kCAEmitterLayerCircle
    layer.emitterMode = kCAEmitterLayerOutline
    layer.renderMode = kCAEmitterLayerAdditive
    layer.emitterZPosition = 100
    layer.preservesDepth = true
    layer.masksToBounds = false
    
    let miniDroplet = CAEmitterCell()
    miniDroplet.contents = UIImage(named:"mini-droplet")?.cgImage
    miniDroplet.birthRate = 5
    miniDroplet.lifetime = 0.3
    miniDroplet.scale = 0.1
    miniDroplet.velocity = -50
    miniDroplet.emissionRange = CGFloat(360).toRadians()
    miniDroplet.scaleRange = 0.1
    miniDroplet.lifetimeRange = 0.1
    miniDroplet.velocityRange = 50
    
    layer.emitterCells = [miniDroplet]
    return layer
  }
}

extension CGFloat {
  func toRadians() -> CGFloat {
    return (self * .pi) / 180
  }
}
