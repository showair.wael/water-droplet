//
//  ViewController.swift
//  Water Droplet
//
//  Created by innovapost on 2018-02-28.
//  Copyright © 2018 innovapost. All rights reserved.
//

import UIKit
import SceneKit
class ViewControllerManual: UIViewController {
  
  @IBOutlet weak var sceneView: SCNView!
  override func viewDidLoad() {
    super.viewDidLoad()
    let rain = SCNParticleSystem(named: "rain", inDirectory: nil)!
    
    let scene = SCNScene()
    sceneView.scene = scene
    
    sceneView.backgroundColor = UIColor.black
    
    let node = createNode()
    
    node.addParticleSystem(rain)
    scene.rootNode.addChildNode(node)
    
    // create and add a camera to the scene
    let cameraNode = createCameraNode(at: SCNVector3(x: 0, y: 0, z: 20)) //z=20
//    scene.rootNode.addChildNode(cameraNode)
    sceneView.pointOfView = cameraNode
    self.view.addSubview(sceneView)
  }
  
  func createNode() -> SCNNode {
    let node = SCNNode()
    
    node.position = SCNVector3(0, 10, 0) //y=10
    
    return node
  }
  
  func createCameraNode(at position: SCNVector3) -> SCNNode {
    let cameraNode = SCNNode()
    cameraNode.camera = SCNCamera()
    // place the camera
    cameraNode.position = position
    return cameraNode
  }
}

